This is a simple mod, which makes your headlights strobe lights!

Controls:
CTRL + S to toggle strobe
CTRL + I to toggle change interval menu
NUMPAD8 to increase interval
NUMPAD2 to decrease interval
(Only the increase/decrease interval keys are changeable)

How to install:
Copy StrobeLights.net.dll to the scripts folder in your GTA IV directory. Make sure you have .NET Scripthook installed.

Notes:
You can only increase/decrease the interval when the change interval menu is open, so don't worry about this conflicting with your Simple Trainer controls.
INI file is created after first run.
You can set if strobe/change interval menu is enabled on startup in the INI file also.

Thanks:
w33dxdrift3r for the idea.

How to compile:
I lost the project files after a hard drive failure, so you'll need to:

* create a new C# library/DLL project in Visual Studio
* replace the generated cs file with the cs file in this archive
* download .NET ScriptHook and add a reference to it
* click compile
* change the extension of the generated .dll file to .net.dll
