﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using GTA;

namespace StrobeLights
{
    public class StrobeLights_Script : Script
    {
        bool strobeOn = false, intervalChangeOn = false;
        GTA.Timer timer;
        int onOff = 1;
        Keys intervalIncrease, intervalDecrease;

        public StrobeLights_Script()
        {
            if (File.Exists(Settings.Filename))
            {
                Settings.Load();
            }
            else
            {
                Settings.SetValue("StrobeEnabledOnStartup", "SETTINGS", false);
                Settings.SetValue("ChangeIntervalEnabledOnStartup", "SETTINGS", false);
                Settings.SetValue("IntervalIncrease", "KEYS", Keys.NumPad8);
                Settings.SetValue("IntervalDecrease", "KEYS", Keys.NumPad2);
            }
            strobeOn = Settings.GetValueBool("StrobeEnabledOnStartup", "SETTINGS", false);
            intervalChangeOn = Settings.GetValueBool("ChangeIntervalEnabledOnStartup", "SETTINGS", false);
            intervalIncrease = Settings.GetValueKey("IntervalIncrease", "KEYS", Keys.NumPad8);
            intervalDecrease = Settings.GetValueKey("IntervalDecrease", "KEYS", Keys.NumPad2);


            PerFrameDrawing += new GraphicsEventHandler(StrobeLights_Script_PerFrameDrawing);

            KeyDown += new GTA.KeyEventHandler(StrobeLights_Script_KeyDown);

            timer = new GTA.Timer();
            timer.Interval = 50;
            timer.Tick += new EventHandler(timer_Tick);
            timer.Start();
        }

        void StrobeLights_Script_PerFrameDrawing(object sender, GraphicsEventArgs e)
        {
            if (intervalChangeOn)
            {
                e.Graphics.DrawText("Interval: " + timer.Interval.ToString(), 20, 20, Color.LimeGreen);
                e.Graphics.DrawText("Press " +  intervalIncrease.ToString() + " to increase interval, and " +  intervalDecrease.ToString() + " to decrease", 20, 40, Color.LimeGreen);
            }
        }

        void StrobeLights_Script_KeyDown(object sender, GTA.KeyEventArgs e)
        {
            if (isKeyPressed(Keys.ControlKey) && e.Key == Keys.S)
            {
                ToggleStrobe();
            }
            else if (isKeyPressed(Keys.ControlKey) && e.Key == Keys.I)
            {
                ToggleIntervalChange();
            }

            if (intervalChangeOn)
            {
                if (e.Key == intervalIncrease)
                {
                    if (timer.Interval < 400)
                    {
                        timer.Interval += 5;
                    }
                }
                else if (e.Key == intervalDecrease)
                {
                    if (timer.Interval > 5)
                    {
                        timer.Interval -= 5;
                    }
                }
            }
        }

        void timer_Tick(object sender, EventArgs e)
        { 
            if (strobeOn)
            {
                Ped plr = Player.Character;
                if (plr.isInVehicle() && plr.isSittingInVehicle())
                {
                    Vehicle veh = plr.CurrentVehicle;

                    if (veh.Exists() && veh != null)
                    {
                        GTA.Native.Function.Call("FORCE_CAR_LIGHTS", veh, onOff);
                        onOff = (onOff > 1) ? 1 : 2; //1 off 2 on
                    }
                }
            }
        }





        void ToggleStrobe()
        {
            if (strobeOn)
            {
                if (Player.Character.isInVehicle()) GTA.Native.Function.Call("FORCE_CAR_LIGHTS", Player.Character.CurrentVehicle, 0); //default light setting
                strobeOn = false;
                Subtitle("Strobe Lights Disabled");
            }
            else
            {
                strobeOn = true;
                Subtitle("Strobe Lights Enabled");
            }
        }
        void ToggleIntervalChange()
        {
            intervalChangeOn = !intervalChangeOn;
            Subtitle("Change interval menu is now " + (intervalChangeOn ? "on" : "off"));
        }


        private static void Subtitle(string info, int millisecs)
        {
            GTA.Native.Function.Call("PRINT_STRING_WITH_LITERAL_STRING_NOW", "STRING", info, millisecs, 1);
        }
        private static void Subtitle(string info)
        {
            GTA.Native.Function.Call("PRINT_STRING_WITH_LITERAL_STRING_NOW", "STRING", info, 1500, 1);
        }
    }
}